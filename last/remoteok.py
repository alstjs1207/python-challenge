
import requests
from bs4 import BeautifulSoup

def extract_job(html):
    title = html.find("td", {"class": "company_and_position"}).find('h2',{"itemprop":"title"}).text
    company = html.find("td", {"class": "company_and_position"}).find('h3',{"itemprop":"name"}).text
    link = html.find("td", {"class": "company_and_position"}).find('a',{"itemprop":"url"}).attrs['href']
    
    return {
        'title': title,
        'company': company,
        "link": f'https://remoteok.io{link}'
    }

def extract_jobs(jobs, URL):
    result = requests.get(URL)
    soup = BeautifulSoup(result.text, "html.parser")
    results = soup.find_all("tr", {"class": "job"})
    for re in results:
        job = extract_job(re)
        jobs.append(job)
    return jobs

def get_jobs(jobs, word):
    print('Scrapping remoteok !!')
    URL = f"https://remoteok.io/remote-dev+{word}-jobs"
    jobs = extract_jobs(jobs, URL)
    return jobs