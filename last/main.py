"""
These are the URLs that will give you remote jobs for the word 'python'

https://stackoverflow.com/jobs?r=true&q=python
https://weworkremotely.com/remote-jobs/search?term=python
https://remoteok.io/remote-dev+python-jobs

Good luck!
"""
from sof import get_jobs as get_sof_jobs
from wework import get_jobs as get_wework_jobs
from remoteok import get_jobs as get_remoteok_jobs
from save import save_to_file_jobs
from flask import Flask, render_template, request, redirect, send_file
import time

app = Flask("LastScrapper")

db = {}


@app.route("/")
def home():
    return render_template('home.html')

@app.route("/report")
def report():
    word = request.args.get('word')
    if word is not None:
        word = word.lower()
        fromDB = db.get(word)
        if fromDB:
            jobs = fromDB
        else:
            jobs = get_sof_jobs(word)
            jobs = get_wework_jobs(jobs, word)
            jobs = get_remoteok_jobs(jobs, word)
            db[word] = jobs
    else:
        return redirect('/')
    return render_template("report.html", 
    searchBy=word, 
    searchByJob=jobs,
    searchByJobNumber=len(jobs))

@app.route("/export")
def export():
    sys_time = time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))
    try:
        word = request.args.get('word')
        if not word:
            raise Exception()
        
        word = word.lower()
        jobs = db.get(word)
        
        if not jobs:
            raise Exception()
       
        save_to_file_jobs(jobs, word, sys_time)
        
        return send_file(f"{word}.csv",mimetype='text/csv',attachment_filename=f"{word}.csv", as_attachment=True, cache_timeout=0)
        #return redirect('/')
    except SystemError:
        print('error',SystemError)
        return redirect('/')


app.run(host="127.0.0.1")
