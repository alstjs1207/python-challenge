
import requests
from bs4 import BeautifulSoup

def extract_job(html):
    title = html.find("span", {"class": "title"}).text
    company = html.find("span", {"class": "company"}).text
    links = html.find_all("a")
    if len(links) > 1:
        link = links[1].attrs['href']
    else:
        link = links[0].attrs['href']

    return {
        'title': title,
        'company': company,
        "link": f'https://weworkremotely.com{link}'
    }

def extract_jobs(jobs, URL):
    result = requests.get(URL)
    soup = BeautifulSoup(result.text, "html.parser")
    results = soup.find_all("li", {"class": "feature"})
    for re in results:
        job = extract_job(re)
        jobs.append(job)
    return jobs

def get_jobs(jobs, word):
    print('Scrapping weworkremotely !!')
    URL = f"https://weworkremotely.com/remote-jobs/search?term={word}"
    jobs = extract_jobs(jobs, URL)
    return jobs