#answer nico
def is_on_list(a_list=[], word=""): #배열과 입력값을 초기화 하는구나 !!기본값 셋팅
  return word in a_list #배열에 있으면 자동으로 True가 되는구나..

def get_x(a_list=[], index=0):
  return a_list[index]

def add_x(a_list=[], word=""):
  a_list.append(word)

def remove_x(a_list=[], word=""):
  a_list.remove(word)

# my coding
# def is_on_list(days, num):
#   result = ''
#   if num in days:
#     result = 'True'
#   else:
#     result = 'False'
#   return result

# def get_x(days,cnt):
#   return days[cnt]

# def add_x(days, day):
#   return days.append(day) --> 리턴을 안해도 되는 이유

# def remove_x(days,day):
#   return days.remove(day) --> 리턴을안해도 되는 이유

# \/\/\/\/\/\/\  DO NOT TOUCH AREA  \/\/\/\/\/\/\ #

days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

print("Is Wed on 'days' list?", is_on_list(days, "Wed"))

print("The fourth item in 'days' is:", get_x(days, 3))

add_x(days, "Sat")
print(days)

remove_x(days, "Mon")
print(days)


# /\/\/\/\/\/\/\ END DO NOT TOUCH AREA /\/\/\/\/\/\/\ #