import os
import requests

'''
#Nico Answer
def restart():
  answer = str(input("Do you want to start over? y/n ")).lower()
  if answer == "y" or answer =="n":
    if answer == "n":
        print("k. bye!")
        return
    elif answer == "y":
      main()
  else:
    print("That's not a valid answer")
    restart()


def main():
  os.system('clear')
  print("Welcome to IsItDown.py!\nPlease write a URL or URLs you want to check. (separated by comma)")
  urls = str(input()).lower().split(",")
  for url in urls:
    url = url.strip()
    if "." not in url:
      print(url, "is not a valid URL.")
    else:
      if "http" not in url:
        url = f"http://{url}"
      try:
        request = requests.get(url)
        if request.status_code == 200:
          print(url,"is up!")
        else:
          print(url, "is down!")
      except:
          print(url, "is down!")
  restart()


main()
'''


# My coding
def program():
  print('Welcome to IsItDown.py!')
  urlData = input('please write a URL or URLS you want to check.(separated by comma)\n')
  urls = urlData.split(",")
  urlRequest(urls)
  exitProgram()
  

def exitProgram():
  exit = input('Do you want to start over? y/n ')
  if exit == 'y':
    os.system('clear')
    program()
  elif exit == 'n':
    print('k.bye!')
    return
  else:
    print("that's not a valid answer")
    exitProgram()

def urlRequest(urls):
  for url in urls:
    url = url.strip()
    #valid check
    validUrl = url.split(".")
    if len(validUrl) > 1:
      if  validUrl[1].endswith(tuple(urlCheck)):
        #http check
        if (url.split('://')[0]).upper() != 'HTTP':
          url = 'http://'+url
        try:
          response = requests.get(url)
          if response.status_code == 200:
            print(f'{url} is up!' )
          else:
            print(f'{url} is down!' )
        except:
            print(f'{url} is down!' )
      else:
        print(f'{url} is not a valid URL.' )
    else:
      print(f'{url} is not a valid URL.' )


urlCheck = {'com','net','co.kr','it'}
program()