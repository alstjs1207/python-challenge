import os
import csv
import requests
from bs4 import BeautifulSoup

#os.system("clear")
alba_url = "http://www.alba.co.kr"

def main():
    jobs = get_company()
    save_to_file_jobs(jobs)
    

def get_company():
    jobs = []
    data = {}
    job_data = {}
    result = requests.get(alba_url)
    if(result.status_code == 200):
        soup = BeautifulSoup(result.text,"html.parser")
        superBrand = soup.find('div',{'id':'MainSuperBrand'})
        box = superBrand.find('ul',{'class':'goodsBox'})
        info = box.find_all("a",{"class":"goodsBox-info"})
        # 많이들 하는 실수 - box.find_all("a",{"class":"goodsBox-info"})["href"]
        # 이런식으로 바로 href를 추출하려고 하는데 find_all은 list이기 때문에 list에서는 데이터를
        # 추출할 수 없다.
        for idx, job in enumerate(info):
            link = job.attrs['href']
            company_title = job.find('span',{'class':'company'}).string
            print(f'start web Scraping ... {idx}/{len(info)} - {company_title}')
            result_a = requests.get(link.strip())

            if(result_a.status_code == 200):
                soup = BeautifulSoup(result_a.text,"html.parser")
                table = soup.find('table')
                rows = table.find_all("tr")[1:]
                job_detail = []
                for row in rows:
                    try:
                        items = row.find_all('td')
                        local = items[0].text.replace('\xa0',' ')
                        title = items[1].find('span',{'class':'company'}).string
                        time = items[2].string
                        pay = items[3].text
                        date = items[4].string
                        job_data = {
                        'local': local,
                        'title': title.strip(),
                        'time' : time,
                        'pay' : pay,
                        'data' : date
                        }
                        print(job_data)
                        job_detail.append(job_data)
                    except:
                        pass

            data = {
                'company' : company_title,
                'link' : link.strip(),
                'content': job_detail
            }
            jobs.append(data)
            
    return jobs


def save_to_file_jobs(jobs):
    for job in jobs:
        company = job['company']
        print('start save [',company,']')
        content = job['content']
        #csv 모듈에서 데이터를 쓸 때 각 라인 뒤에 빈 라인이 추가되는것을 방지하기 위해 newline='' 작성
        file = open(f"D:\dsmpc\workspace\python-challenge\company\{company}.csv", mode="w", encoding="UTF8", newline='') 
        writer = csv.writer(file)
        writer.writerow(["place","title","time","pay","date"])
        for info in content:
            writer.writerow(list(info.values()))




main()