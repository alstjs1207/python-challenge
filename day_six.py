import os
import requests
from bs4 import BeautifulSoup
from babel.numbers import format_currency
#os.system("clear")

"""
Use the 'format_currency' function to format the output of the conversion
format_currency(AMOUNT, CURRENCY_CODE, locale="ko_KR" (no need to change this one))
"""

"""
# Nico Answer
code_url = "https://www.iban.com/currency-codes"
currency_url = "https://transferwise.com/gb/currency-converter/"


countries = []

codes_request = requests.get(code_url)
codes_soup = BeautifulSoup(codes_request.text, "html.parser")

table = codes_soup.find("table")
rows = table.find_all("tr")[1:]

for row in rows:
  items = row.find_all("td")
  name = items[0].text
  code =items[2].text
  if name and code:
    if name != "No universal currency":
      country = {
        'name':name.capitalize(),
        'code': code
      }
      countries.append(country)


def ask_country(text):
  print(text)
  try:
    choice = int(input("#: "))
    if choice > len(countries):
      print("Choose a number from the list.")
      return ask_country(text)
    else:
      print(f"{countries[choice]['name']}")
      return countries[choice]
  except ValueError:
    print("That wasn't a number.")
    return ask_country(text)


def ask_amount(a_country, b_country):
  try:
    print(f"\nHow many {a_country['code']} do you want to convert to {b_country['code']}?")
    amount = int(input())
    return amount
  except ValueError:
    print("That wasn't a number.")
    return ask_amount(a_country, b_country)
  


print("Welcome to CurrencyConvert PRO 2000\n")
for index, country in enumerate(countries):
  print(f"#{index} {country['name']}")

user_country = ask_country("\nWhere are you from? Choose a country by number.\n")
target_country = ask_country("\nNow choose another country.\n")


amount = ask_amount(user_country, target_country)

from_code = user_country['code']
to_code = target_country['code']

currency_request = requests.get(f"{currency_url}{from_code}-to-{to_code}-rate?amount={amount}")
currency_soup = BeautifulSoup(currency_request.text, "html.parser")
result = currency_soup.find("input", {"id":"cc-amount-to"})
if result:
  result = result['value']
  amount = format_currency(amount, from_code, locale="ko_KR")
  result = format_currency(result, to_code, locale="ko_KR")
  print(f"{amount} is {result}")

"""


url = "https://www.iban.com/currency-codes"
Flag = False
CURRENCY_CODE = {}

def main():
  print('Welcome to CurrencyConvert PRO 2000')
  #get_country
  countryList = get_country(url)
  for idx,country in enumerate(countryList):
      print(f"#{idx} {country['country']}")
  print('\n Where are you from? choose a country by number.\n')
  execute(countryList,Flag)

# transferWise web Scraping
def get_convert(money):
  
  transferUrl = f'https://transferwise.com/gb/currency-converter/{CURRENCY_CODE[1].lower()}-to-{CURRENCY_CODE[2].lower()}-rate?amount={money}'
  result = requests.get(transferUrl)
  if(result.status_code == 200):
    soup = BeautifulSoup(result.text,"html.parser")
    caculator = soup.find('div',{"class":"js-Calculator cc__header cc__header-spacing card card--with-shadow m-b-5"})
    rate = caculator.find("input", attrs={"id":"rate"})["value"] #rate 추출
    ConvertedtoMoney = (money * float(rate))
  return ConvertedtoMoney

def exchangeMoney():
  print(f'How many {CURRENCY_CODE[1]} do you want to convert to {CURRENCY_CODE[2]}?')
  try:
    money = int(input())
    change_Money = get_convert(money) #transferWise web Scraping
    country_a = format_currency(money, CURRENCY_CODE[1], locale="ko_KR")
    country_b = format_currency(change_Money, CURRENCY_CODE[2], locale="ko_KR")
    print(f'{country_a} is {country_b}')
  except:
    print("That wasn't a number.\n")
    exchangeMoney()

def execute(Countries,Flag):
  try:
    word = int(input('#:'))
    #올바른 숫자를 입력한 경우
    if word < len(Countries):
      country = Countries[word]['country']
      countryCode = Countries[word]['code']
      print(country,'\n')

      #두번째 도시 선택
      if Flag is True:
        CURRENCY_CODE[2] = countryCode
        exchangeMoney() #화폐 변환

      #첫번째 도시 선택  
      else:
        CURRENCY_CODE[1] = countryCode
        print("Now choose another country.\n")
        execute(Countries,True)
    #없는 숫자의 경우
    else:
      print("Choose a number from the list.")
      execute(Countries,False)
  except:
    print("That wasn't a number.")
    execute(Countries,False)

def get_country(url):
  Countries = []
  country_data = {}
  result = requests.get(url)
  if(result.status_code == 200):
    soup = BeautifulSoup(result.text,"html.parser")
    table = soup.find('table',{"class":"table table-bordered downloads tablesorter"}).find('tbody')
    data_tr = table.find_all('tr')

    for data in data_tr:
      data_td = data.find_all('td')
      country = data_td[0].string
      code = data_td[2].string
      if code is not None:
        country_data = {
          'country': country.capitalize(),
          'code': code
        }
        Countries.append(country_data)

  return Countries

main()