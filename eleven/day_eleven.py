import requests
from bs4 import BeautifulSoup
from flask import Flask, render_template, request

"""
When you try to scrape reddit make sure to send the 'headers' on your request.
Reddit blocks scrappers so we have to include these headers to make reddit think
that we are a normal computer and not a python script.
How to use: requests.get(url, headers=headers)
"""

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}


"""
All subreddits have the same url:
i.e : https://reddit.com/r/javascript
You can add more subreddits to the list, just make sure they exist.
To make a request, use this url:
https://www.reddit.com/r/{subreddit}/top/?t=month
This will give you the top posts in per month.
"""

subreddits = [
    "javascript",
    "reactjs",
    "reactnative",
    "programming",
    "css",
    "golang",
    "flutter",
    "rust",
    "django"
]

app = Flask("DayEleven")

URL = 'https://www.reddit.com/r/'

def extract_data(html, db, word):
    dataCheck = html.find("span",{"class":"_2oEYZXchPfHwcf9mTMGMg8"}) #promoted
    if dataCheck is None:
        vote = html.find("div",{"class":"_1rZYMD_4xY3gRcSS3p8ODO"}).text
        if "." in vote:
            sort = float(vote.replace("k","")) * 1000
        else:
            sort = float(vote)

        title = html.find("h3",{"class":"_eYtD2XCVieq6emjKBH3m"}).text
        link = html.find("a").attrs['href']

        searchData = {
            "sort": sort,
            "vote": vote,
            "title": title,
            "link": link,
            "subreddit": word
        }
        db.append(searchData)

    return db

@app.route("/")
def home():
    return render_template('home.html', data=subreddits)


@app.route("/read")
def read():
    wordDB = []
    db = []
    for subreddit in subreddits:
        if "on" == request.args.get(subreddit):
            word = f'r/{subreddit}'
            wordDB.append(word)
            result = requests.get(f"{URL}{subreddit}/top/?t=month",headers=headers)
            soup = BeautifulSoup(result.text, "html.parser")
            results = soup.find_all("div",{"class":"Post"})
            for res in results:
                data = extract_data(res, db, word)
    
    return render_template('read.html',searchByData=data, search=wordDB)

app.run(host="127.0.0.1")