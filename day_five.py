import os
import requests
from bs4 import BeautifulSoup

"""
Hint2 - index in 'for' loops? : https://stackoverflow.com/questions/522563/accessing-the-index-in-for-loops

"""

"""
#Nico answer
url = "https://www.iban.com/currency-codes"


countries = []

request = requests.get(url)
soup = BeautifulSoup(request.text, "html.parser")

table = soup.find("table")
rows = table.find_all("tr")[1:]

for row in rows:
  items = row.find_all("td")
  name = items[0].text
  code =items[2].text
  if name and code:
    if name != "No universal currency":
      country = {
        'name':name.capitalize(),
        'code': code
      }
      countries.append(country)


def ask():
  try:
    choice = int(input("#: "))
    if choice > len(countries):
      print("Choose a number from the list.")
      ask()
    else:
      country = countries[choice]
      print(f"You chose {country['name']}\nThe currency code is {country['code']}")
  except ValueError:
    print("That wasn't a number.")
    ask()


print("Hello! Please choose select a country by number:")
for index, country in enumerate(countries):
  print(f"#{index} {country['name']}")

ask()
"""

#os.system("clear")
url = "https://www.iban.com/currency-codes"

def main():
  print('Hello! Please choose select a country by number:')
  Countries = get_country(url)
  execute(Countries)
  

def execute(Countries):
  try:
    word = int(input('#:'))
    #올바른 숫자를 입력한 경우
    if Countries.get(word):
      country = Countries.get(word)['country']
      countryCode = Countries.get(word)['code']
      print("You choose",country,"\nThe currency code is",countryCode)
    #없는 숫자의 경우
    else:
      print("Choose a number from the list.")
      execute(Countries)
  except:
    print("That wasn't a number.")
    execute(Countries)

def get_country(url):
  Countries = {}
  country_data = {}
  result = requests.get(url)
  if(result.status_code == 200):
    soup = BeautifulSoup(result.text,"html.parser")
    table = soup.find('table',{"class":"table table-bordered downloads tablesorter"}).find('tbody')
    data_tr = table.find_all('tr')
    cnt = 0
    for data in data_tr:
      data_td = data.find_all('td')
      country = data_td[0].string
      country = country.capitalize()
      code = data_td[2].string
      if code is not None:
        country_data = {
          'country': country,
          'code': code
        }
        print(f'# {cnt} {country}')
        Countries[cnt] = country_data
        cnt  += 1

  return Countries

main()