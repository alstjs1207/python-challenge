<html>
    <head>
        <title>Nomad News | {{fullData.title}}</title>
        <link href="https://andybrewer.github.io/mvp/mvp.css" rel="stylesheet"></link>
        <link rel="icon" href="data:;base64,iVBORw0KGgo=">
    </head>
    <body>
        <header>
            <h1>{{fullData.title}}</h1>
            <div>
                {{fullData.points}} points | By {{fullData.author}} | <a href={{fullData.url}} target="_blanl">{{fullData.url}}</a>
            </div>
        </header>
        <main>
            {% for news in fullData.children %}
                {% if news.author == None %}
                    <div> [deleted] </div>
                    <hr>
                {% else %}
                    <strong>{{news.author}}:</strong>
                    <p></p>
                    {{news.text|safe}}
                    <p></p>
                    <hr>
                {% endif %}
            {% endfor %}
            </main>
    </body>
</html>