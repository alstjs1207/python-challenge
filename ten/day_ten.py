import requests
from flask import Flask, render_template, request

# This function makes the URL to get the detail of a storie by id.
# Heres the documentation: https://hn.algolia.com/api

app = Flask("DayNine")

commentdb = {}
C_FAKE_DB = []

loaddb = {}
L_FAKE_DB = []

base_url = "http://hn.algolia.com/api/v1"

def make_detail_url(id):
  return f"{base_url}/items/{id}"

def get_url(word):
  #base_url = "http://hn.algolia.com/api/v1"

  if word == 'new':
    # This URL gets the newest stories.
    url = f"{base_url}/search_by_date?tags=story"
  else:
    # This URL gets the most popular stories
    url = f"{base_url}/search?tags=story"

  return url

def get_hackernews(url, word):
  newsList = []
  res = requests.get(url).json()
  jsonData = res['hits']

  # json으로 잘되어있는 데이터를 일부러 쪼개서 다시 만들었네.....
  # for data in jsonData:
  #   id = data['objectID']
  #   news_data = {
  #     "id" : id,
  #     "title" : data['title'],
  #     "url" : data['url'],
  #     "points" : data['points'],
  #     "author" : data['author'],
  #     "comments" : data['num_comments']
  #   }
  #   newsList.append(news_data)
  #   loaddb[word] = newsList
  loaddb[word] = jsonData

  return jsonData

def get_hackernews_comments(url):
  comments = []
  res = requests.get(url).json()
  id = res['id']
  # 여기도 마찬가지로 굳이 또 쪼개서 넣고있네....
  #jsonData = res['children']
  # for data in jsonData:
  #   if data['author'] is None:
  #     comment = {
  #       "author":"deleted",
  #       "text":"",
  #     }
  #   else:  
  #     comment = {
  #       "author" : data['author'],
  #       "text" : data['text']
  #     }

  #   comments.append(comment)

  # detail = {
  #   "title" : res['title'],
  #   "points" : res['points'],
  #   "author" : res['author'],
  #   "url" : res['url'],
  #   "comments" : comments
  # }
  # commentdb[id] = detail
  commentdb[id] = res
  return res

@app.route("/")
def home():
  word = request.args.get('order_by')
  if word is None:
    word = 'popular'
  
  L_FAKE_DB = loaddb.get(word)
  if L_FAKE_DB:
    dataNews = L_FAKE_DB
  else:
    url = get_url(word)
    dataNews = get_hackernews(url, word)
  
  return render_template('index.html', fullData=dataNews,orderBy=word)


@app.route("/<id>")
def menu(id):
  C_FAKE_DB = commentdb.get(int(id)) #id가 int가 아니여서 검색이 안되었음
  if C_FAKE_DB:
    newsDetail = C_FAKE_DB
  else:
    url = make_detail_url(id)
    newsDetail = get_hackernews_comments(url)

  #return render_template('detail.html', fullData=newsDetail['comments'], newsData=newsDetail)
  return render_template('detail.html', fullData=newsDetail)

app.run(host="127.0.0.1")